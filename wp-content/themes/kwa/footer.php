<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package kwa
 */

?>

	<footer id="colophon" class="site-footer">
		<div class="background-image">
			<div class="blue-overlay">
				<div class="site-info">
					<div class="footer-menu">
						<?php
				        wp_nav_menu( array(
				            'theme_location' => 'menu-1', // Defined when registering the menu
				            'menu_id'        => 'primary-menu',
				        ) );
				        ?>
				    </div>
					<div class="water-trail-map">
						<div class="image-container">
							<img src=<?php echo get_template_directory_uri() . "/images/water-trail-map.png"; ?> />
						</div>
						<div class="text-wrap">
							<h3>Water Trail Map</h3>
							<p>The water trail provides fun for all whether you are a beginner or a veteran.</p>
							<a href="#" class="orange-button">Download</a>
						</div>
					</div>
					<div class="footer-right-content">
						<div class="text-wrap">
							<a>info@kearneywhitewater.org</a>
						</div>
						<div class="donate-button-container">
							<p class="donate-button">DONATE</p>
							<div class="donate-line"></div>
						</div>
						<div class="social-icons-container">
							<a href="https://www.instagram.com/kearneywhitewater/"><div class="icon-container"><i class="fab fa-instagram-square"></i></div></a>
							<a href="https://twitter.com/krnywhitewater"><div class="icon-container"><i class="fab fa-twitter-square"></i></div></a>
							<a href="https://www.facebook.com/KearneyWhitewater/"><div class="icon-container"><i class="fab fa-facebook-square"></i></div></a>
						</div>
					</div>
				</div><!-- .site-info -->
				<div class="bottom-info-container">
					<p>Copyright &#169; <span class="year"><?php echo date('Y'); ?></span> | Kearney Whitewater Association | Designed and Developed by RadStad Marketing</p> 
				</div>
			</div><!-- .blue-overlay -->
		</div><!-- .background-image -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
