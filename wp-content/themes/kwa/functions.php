<?php
/**
 * kwa functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package kwa
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'kwa_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function kwa_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on kwa, use a find and replace
		 * to change 'kwa' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'kwa', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'kwa' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'kwa_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'kwa_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function kwa_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'kwa_content_width', 640 );
}
add_action( 'after_setup_theme', 'kwa_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function kwa_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'kwa' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'kwa' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'kwa_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function kwa_scripts() {
	wp_enqueue_style( 'kwa-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_enqueue_style( 'swiper-styles', "https://unpkg.com/swiper/swiper-bundle.min.css", array(), _S_VERSION );
	wp_enqueue_style( 'google-fonts', "https://fonts.gstatic.com", array(), _S_VERSION );
	wp_enqueue_style( 'montserrat-font', "https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800&display=swap", array(), _S_VERSION );
	wp_enqueue_style( 'archivo-font', "https://fonts.googleapis.com/css2?family=Archivo:wght@400;500;600;700&display=swap", array(), _S_VERSION );

	
	wp_style_add_data( 'kwa-style', 'rtl', 'replace' );

	wp_enqueue_script( 'kwa-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );
	wp_enqueue_script("swiper-js", "https://unpkg.com/swiper/swiper-bundle.min.js", array(), _S_VERSION, true );
	wp_enqueue_script("four-across-slider", get_template_directory_uri() . '/js/swiper-four-across.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'kwa_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

//Registering ACF Blocks

add_action('acf/init', 'my_acf_init_block_types');
function my_acf_init_block_types() {

	// check function exists

	if( function_exists('acf_register_block_type') ) {

		/*register hero section*/
		acf_register_block_type(array(
			'name'				=> 'hero-with-side-text',
			'title'				=> __('Full Width Image with Side Text'),
			'description'		=> __('A full width image with a color and text overlay.'),
			'render_template'	=> 'template-parts/blocks/content-herowithsidetext-section.php',
			'category'			=> 'formatting',
			'icon'				=> 'desktop',
			'keywords'			=> array( 'full', 'hero', 'wide' ),
			'mode'				=> 'auto',
			'align'				=> 'full',
			'example'           => array(
				'attributes'		=> array(
					'mode'				=> 'preview',
					'data'				=> array(
						'background_photo'		=> ["url" => "https://images.unsplash.com/photo-1455577380025-4321f1e1dca7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60"],
						'header'				=> "Header For Site",
						'button_text'			=> "Button",
						'button_link'			=> "https://radstadmarketing.com",
						'is_preview'			=> true
					)
				)
			)
		));

		acf_register_block_type(array(
			'name'				=> 'four-across-slider',
			'title'				=> __('Slider with four posts across'),
			'description'		=> __('A slider that displays four posts across'),
			'render_template'	=> 'template-parts/blocks/4-across-slider-section.php',
			'category'			=> 'formatting',
			'icon'				=> 'desktop',
			'keywords'			=> array( 'full', 'slider', 'four across' ),
			'mode'				=> 'auto',
		));

		acf_register_block_type(array(
			'name'				=> 'centered-text',
			'title'				=> __('Centered text'),
			'description'		=> __('A block that displays centered text'),
			'render_template'	=> 'template-parts/blocks/centered-text.php',
			'category'			=> 'formatting',
			'icon'				=> 'desktop',
			'keywords'			=> array( 'full', 'centered', 'text' ),
			'mode'				=> 'auto',
		));

		acf_register_block_type(array(
			'name'				=> 'full-width-image',
			'title'				=> __('Full Width Image'),
			'description'		=> __('A block that displays a full width image'),
			'render_template'	=> 'template-parts/blocks/full-width-image.php',
			'category'			=> 'formatting',
			'icon'				=> 'desktop',
			'keywords'			=> array( 'full', 'image' ),
			'mode'				=> 'auto',
		));

		acf_register_block_type(array(
			'name'				=> 'image-above-centered-text',
			'title'				=> __('Image above centered text'),
			'description'		=> __('A block that displays an image above centered text'),
			'render_template'	=> 'template-parts/blocks/image-above-centered-text.php',
			'category'			=> 'formatting',
			'icon'				=> 'desktop',
			'keywords'			=> array( 'full', 'centered', 'text', 'image', 'above' ),
			'mode'				=> 'auto',
		));

		acf_register_block_type(array(
			'name'				=> 'image-left-text-right',
			'title'				=> __('Image Left Text Right'),
			'description'		=> __('A block that displays an image on the left and text, with an optional button, on the right.'),
			'render_template'	=> 'template-parts/blocks/image-left-text-right.php',
			'category'			=> 'formatting',
			'icon'				=> 'desktop',
			'keywords'			=> array( 'full', 'text', 'image', 'button' ),
			'mode'				=> 'auto',
		));

		acf_register_block_type(array(
			'name'				=> 'text-left-full-image-right',
			'title'				=> __('Text Left Full Image Right'),
			'description'		=> __('A block that displays text, with an optional button, on the left and a full width image on the right.'),
			'render_template'	=> 'template-parts/blocks/text-left-full-image-right.php',
			'category'			=> 'formatting',
			'icon'				=> 'desktop',
			'keywords'			=> array( 'full', 'text', 'image', 'button' ),
			'mode'				=> 'auto',
		));

		acf_register_block_type(array(
			'name'				=> 'number-left-text-right',
			'title'				=> __('Number Left Text Right'),
			'description'		=> __('A block that displays a big number on the left and text on the right.'),
			'render_template'	=> 'template-parts/blocks/number-left-text-right.php',
			'category'			=> 'formatting',
			'icon'				=> 'desktop',
			'keywords'			=> array( 'number', 'text' ),
			'mode'				=> 'auto',
		));

	}
}

require get_template_directory() . '/bootstrap/bootstrap-navwalker.php';