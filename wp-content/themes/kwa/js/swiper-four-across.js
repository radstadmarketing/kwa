var swiper = new Swiper('.swiper-container-four-across', {
      slidesPerView: 4,
      spaceBetween: 8,
      slidesPerGroup: 4,
      // init: false,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      breakpoints: {
        540: {
          slidesPerView: 2,
          spaceBetween: 4,
          slidesPerGroup: 2
        },
        720: {
          slidesPerView: 3,
          spaceBetween: 6,
          slidesPerGroup: 3
        },
        960: {
          slidesPerView: 4,
          spaceBetween: 8,
          slidesPerGroup: 4
        }
      }
    });