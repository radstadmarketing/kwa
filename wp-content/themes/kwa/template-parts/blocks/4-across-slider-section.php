<?php
/**
 * Block Name: Four Across Responsive Slider 
 *
 * This is the template that displays a full width responsive slider that at full width shows 4 slides across. (Max 12 slides) 
**/

	//Get Post Type to Display
	$postType = get_field('post_type');
	if($postType == null){
		$postType = 'post';
	}

	//The query for Post Type
	$the_query = new WP_Query( array(	'post_type' => $postType,
									'post_status' => 'publish',
									'posts_per_page' => '12' ) );


?>
<section class="four-across-slider">
	<h2><?php echo $postType?></h2>
	<div class="swiper-container swiper-container-four-across">
		    <div class="swiper-wrapper">
		    	<?php if ( $the_query->have_posts() ) {
				    while ( $the_query->have_posts() ) {
				        $the_query->the_post(); ?>
				        <div class="swiper-slide"> 
				        	<!-- TODO: Make Sliders a link-->
				        	<div class="post-image">
				        		<?php if ( has_post_thumbnail() ) {
								the_post_thumbnail();
								} else { ?>
								<img src="<?php bloginfo('template_directory'); ?>/images/feature-2-cropped.jpg" alt="<?php the_title(); ?>" />
								<?php } ?>
							</div>
				        	<div class="post-content">
					        	<div class="post-title">
					        		<h3><?php the_title(); ?></h3>
					        	</div>
					        	<div class="post-text">
					        	<p><?php the_excerpt(); ?></p>
					        	</div>
				        	</div>
		      			</div>
				        
				    <?php }
				} else {
				    // no posts found
				}
				/* Restore original Post Data */
				wp_reset_postdata(); ?>
		      
		    </div>

		    <div class="swiper-button-prev"></div>
    		<div class="swiper-button-next"></div>
  		</div>
</section>