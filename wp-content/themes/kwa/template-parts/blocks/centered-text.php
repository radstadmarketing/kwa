<?php
/**
 * Block Name: Centered text 
 * This is the template that displays centered text.
 */



?>

<section class="centered-text">
  <div class="background">
    <h2><?php the_field('heading'); ?></h2>
    <p><?php the_field('body'); ?></p>
  </div>
</section>
