<?php
/**
 * Block Name: Hero With Side Text 
 *
 *
 * This is the template that displays a full width image with a black to transparent gradient. 
**/
?>
<section class="hero-with-side-text">
	<div class="hero">
		<?php

		$image = get_field('background_photo');
		if($image == null){
			$image = "https://images.unsplash.com/photo-1455577380025-4321f1e1dca7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60";
		}

		$buttonLink = get_field('button_link');
		$buttonText = get_field('button_text');
		?>

		<div class="hero-img" style="background-image: url('<?php echo $image; ?>')">
			<div class="infowrap">
				<div class="dark-overlay">
					<div class="hero-textwrap">
						<h1><?php the_field('header'); ?></h1>
						<?php if($buttonLink != null && $buttonText != null): ?>
							<a href="<?php the_field('button_link'); ?>" class="orange-button" ><?php the_field('button_text'); ?></a>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>