<?php
/** 
 * Block Name: Full Width Image
 * This is the template that displays a full width image.
 */

$image = get_field('image');


?>

<section class="full-width-image">
  <div class="background">
    <?php if( !empty( $image ) ): ?>
      <img src=<?php echo $image; ?> />
    <?php endif; ?>
  </div>
</section>
