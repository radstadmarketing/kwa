<?php
/**
 * Block Name: Image above centered text
 * This is the template that displays an image above centered text.
 */



?>

<section class="image-above-centered-text">
  <div class="background">
  	<?php if( get_field('image') ): ?>
  		<div class="image-container">
	    	<img src="<?php the_field('image'); ?>" />
	    </div>
	<?php endif; ?>
    <h2><?php the_field('heading'); ?></h2>
    <p><?php the_field('body'); ?></p>
  </div>
</section>
