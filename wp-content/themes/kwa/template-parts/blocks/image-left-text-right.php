<?php
/**
 * Block Name: Image Left Text Right
 * This is the template that displays an image on the left and text with an optional button.
 */ 

$buttonLink = get_field('button_link');
$buttonText = get_field('button_text');

?>

<section class="image-left-text-right">
  <div class="background">
  	<?php if( get_field('image') ): ?>
  		<div class="image-container">
	    	<img src="<?php the_field('image'); ?>" />
	    </div>
	   <?php endif; ?>
    <div class="text-wrap">
      <h2><?php the_field('heading'); ?></h2>
      <p><?php the_field('body'); ?></p>
      <?php if($buttonLink != null && $buttonText != null): ?>
    		<a href="<?php the_field('button_link'); ?>" class="orange-button" ><?php the_field('button_text'); ?></a>
    	<?php endif; ?>
    </div>
  </div>
</section>
