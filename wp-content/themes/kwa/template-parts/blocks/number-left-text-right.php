<?php
/**
 * Block Name: Number Left Text Right
 * This is the template that displays an image on the left and text with an optional button.
 */ 


?>

<section class="number-left-text-right">
  <div class="background" style="background-color:<?php the_field('background_color'); ?>">
    <div class="number-wrap">
      <p class="number"><?php the_field('number'); ?></p> 
    </div>
     <!--<div class="arrow">
       <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 116.02 500"><defs><style>.arrow-2{fill:rgba(75,75,75,0.2);}</style></defs><polygon class="arrow-2" points="116.02 250 0 0 0 500 116.02 250"/></svg>
     </div>-->
    <div class="text-wrap">
      <h2><?php the_field('heading'); ?></h2>
      <h3><?php the_field('subheading'); ?></h3>
      <p><?php the_field('body'); ?></p>
    </div>
  </div>
</section>
