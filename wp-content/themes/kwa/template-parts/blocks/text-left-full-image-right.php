<?php
/**
 * Block Name: Text Left Full Image Right
 * A block that displays text, with an optional button, on the left and a full width image on the right.
 */ 

$buttonLink = get_field('button_link');
$buttonText = get_field('button_text');

?>

<section class="text-left-full-image-right">
  <div class="background">
    <div class="text-wrap">
      <h2><?php the_field('heading'); ?></h2>
      <p><?php the_field('body'); ?></p>
      <?php if($buttonLink != null && $buttonText != null): ?>
    		<a href="<?php the_field('button_link'); ?>" class="orange-button" ><?php the_field('button_text'); ?></a>
    	<?php endif; ?>
    </div>
    <?php if( get_field('image') ): ?>
      <div class="image-container">
        <img src="<?php the_field('image'); ?>" />
      </div>
     <?php endif; ?>
  </div>
</section>
